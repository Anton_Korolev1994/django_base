from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'news'
    verbouse_name = 'Новости'
