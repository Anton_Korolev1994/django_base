from django.shortcuts import render

from .models import New


def index(request):
    news = New.objects.all()
    context = {'news': news,
               'title': 'Список новостей'
               }
    return render(request, template_name='news/index.html', context=context)
