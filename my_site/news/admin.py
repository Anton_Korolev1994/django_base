from django.contrib import admin
from .models import New, Category


class ExpiryDateFilter(admin.SimpleListFilter):
    title = 'Дата публикации'
    parameter_name = 'created_at'

    def lookups(self, request, model_admin):
        return (
            ('january', 'Январь'),
            ('february', 'Февраль'),
            ('march', 'Март'),
            ('april', 'Апрель'),
            ('may', 'Май'),
            ('june', 'Июнь'),
            ('july', 'Июль'),
            ('august', 'Август'),
            ('september', 'Сентябрь'),
            ('october', 'Октябрь'),
            ('november', 'Ноябрь'),
            ('december', 'Декабрь'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'january':
            return queryset.filter(created_at__month=1)
        elif self.value() == 'february':
            return queryset.filter(created_at__month=2)
        elif self.value() == 'march':
            return queryset.filter(created_at__month=3)
        elif self.value() == 'april':
            return queryset.filter(created_at__month=4)
        elif self.value() == 'may':
            return queryset.filter(created_at__month=5)
        elif self.value() == 'june':
            return queryset.filter(created_at__month=6)
        elif self.value() == 'july':
            return queryset.filter(created_at__month=7)
        elif self.value() == 'august':
            return queryset.filter(created_at__month=8)
        elif self.value() == 'september':
            return queryset.filter(created_at__month=9)
        elif self.value() == 'october':
            return queryset.filter(created_at__month=10)
        elif self.value() == 'november':
            return queryset.filter(created_at__month=11)
        elif self.value() == 'december':
            return queryset.filter(created_at__month=12)
        else:
            return queryset


class NewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'category', 'created_at', 'updated_at', 'is_published')
    list_display_links = ('id', 'title')
    search_fields = ('title', 'content')
    list_editable = ('is_published',)
    list_filter = ('is_published', 'category', ExpiryDateFilter)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    list_display_links = ('id', 'title')
    search_fields = ('title',)


admin.site.register(New, NewsAdmin)
admin.site.register(Category, CategoryAdmin)
